class CustomError extends Error {
    constructor({ message, errorName, statusCode, data }) {
        super(message);

        this.name = errorName;
        this.statusCode = statusCode;
        this.data = data;
        Error.captureStackTrace(this, CustomError);
    }

    serializeErrors() {
        return {
            message: this.message,
            data: this.data
        }
    }
}

module.exports = {
    CustomError
}