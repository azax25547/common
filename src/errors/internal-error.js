const { CustomError } = require("./custom-error");

class InternalServerError extends CustomError {
    constructor(statusCode, message = "Bad Request") {
        if (message === null || message === "")
            message = "Bad Request"
        super({
            message,
            statusCode: statusCode || 501,
            errorName: "InternalServerError"
        })
    }
}


module.exports = {
    InternalServerError
}