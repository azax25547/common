const { CustomError } = require("./custom-error");

class HTTPError extends CustomError {
    constructor(message, data, statusCode) {
        if (message === null || message === "")
            message = "Bad Request"
        super({
            message,
            data,
            statusCode: statusCode || 404,
            errorName: "HTTPError"
        })
    }
}


module.exports = {
    HTTPError
}