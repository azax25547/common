const { CustomError } = require("./custom-error");

class DatabaseError extends CustomError {
    constructor(data, message = "Bad Request") {
        super({
            message,
            data,
            statusCode: 501,
            errorName: "DatabaseError"
        })
    }
}


module.exports = {
    DatabaseError
}