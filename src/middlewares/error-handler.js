const { CustomError } = require("../errors/custom-error");

const errorhandler = (err, req, res, next) => {
    if (err instanceof CustomError) {
        return res.status(err.statusCode).send({ errors: err.serializeErrors() })

    }

    return res.status(501).send({
        errors: {
            message: "Something went Wrong"
        }
    })
}


module.exports = {
    errorhandler
}