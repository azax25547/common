const jwt = require("jsonwebtoken");
const { HTTPError } = require("../errors/Http-error");

const authAPIs = (req, res, next) => {
    let token = req.headers['x-access-token'];
    if (token === null) {
        throw new HTTPError('No token provided.', "", 403);
    }
    jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
        if (err) throw new HTTPError('Failed to authenticate token.', '', 403);
        req.user = decoded;
        return next();
    })
}

module.exports = { authAPIs };