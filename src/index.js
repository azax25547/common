const { CustomError } = require('./errors/custom-error');
const { DatabaseError } = require('./errors/database-error');
const { HTTPError } = require('./errors/Http-error');
const { InternalServerError } = require('./errors/internal-error');

const { authAPIs } = require('./middlewares/auth');
const { errorhandler } = require('./middlewares/error-handler')

module.exports = {
    errors: {
        CustomError,
        DatabaseError,
        HTTPError,
        InternalServerError
    },
    middlewares: {
        authAPIs,
        errorhandler
    },
}